#ifndef __PROMPT_H__
#define __PROMPT_H__

#include <stdint.h>

#include "bitmaps.h"

extern const BITMAP bmp_battery_100;
extern const BITMAP bmp_battery_66;
extern const BITMAP bmp_battery_50;
extern const BITMAP bmp_battery_33;
extern const BITMAP bmp_battery_0;
extern const BITMAP bmp_battery_charging;
extern const BITMAP bmp_ble_64;
extern const BITMAP bmp_nfc_64;
extern const BITMAP bmp_usb_64;
extern const BITMAP bmp_ble_link;
extern const BITMAP bmp_cn_active_success;
extern const BITMAP bmp_cn_BixinKEY_active_success;
extern const BITMAP bmp_cn_ble_link;
extern const BITMAP bmp_cn_button_yes_no;
extern const BITMAP bmp_cn_computerlink;
extern const BITMAP bmp_cn_confirm_pin;
extern const BITMAP bmp_cn_confirm_pubkey;
extern const BITMAP bmp_cn_input_pin;
extern const BITMAP bmp_cn_nfc_link;
extern const BITMAP bmp_cn_poweroff;
extern const BITMAP bmp_cn_prikey_gen;
extern const BITMAP bmp_cn_sign_ok;
extern const BITMAP bmp_cn_sign_ok_view;
extern const BITMAP bmp_cn_sign_success_gohome;
extern const BITMAP bmp_cn_gohome;
extern const BITMAP bmp_cn_sign_success_phone;
extern const BITMAP bmp_cn_sn;
extern const BITMAP bmp_cn_touch_phone;
extern const BITMAP bmp_cn_unactive;
extern const BITMAP bmp_cn_update_sucess;
extern const BITMAP bmp_cn_updating_notpower_off;
extern const BITMAP bmp_cn_updown_view;
extern const BITMAP bmp_cn_usb_link;
extern const BITMAP bmp_cn_version;
extern const BITMAP bmp_logo;
extern const BITMAP bmp_update;
extern const BITMAP bmp_cn_export_encrypted_prikey;
extern const BITMAP bmp_cn_import_prikey;
extern const BITMAP bmp_cn_update_settings;
extern const BITMAP bmp_cn_BixinKEY_initialized;
extern const BITMAP bmp_cn_view_hw_info;
extern const BITMAP bmp_cn_res_settings_or_not;
extern const BITMAP bmp_cn_has_res_settings;
extern const BITMAP bmp_logo_BixinKEY;

#endif
